## What I Did
* Renaming AuditVirementRepository.java to AuditTransferRepository.java.
* Creating Deposit Repo.
* Creating Deposit Dto.
* Creating mappers
* implementing a service layer for Audit, Deposit and Transfer.
    * no service layer is implemented for the User case (there is no much business logic code concerning the user part).
* Creating controllers.
* making use of the existing exceptions in the BL.
* Creating a simple test.



| Endpoints  | Accepted HTTP Verbs |
| ------------- | ------------- |
| utilisateurs/  | GET  |
| comptes/  | GET  |
| transfers/  | GET, POST  |
| deposits/  | GET, POST  |

**Things I haven't done but I think they worth mentioning :**

* for the audit part I guess it's better to have only one table since there is a column called EVENTTYPE that make us differnciate between various types of transactions.
* Also, I think It's better not to store a full message in the db (MESSAGE Column in this case), this is a calculated value, storing calculated values in the db is a violation of database normalization.

