package ma.octo.assignement;

import java.math.BigDecimal;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.TransferRepository;

public class TransactionTest {
    

    @Test
    public void executeTransfer() {
  
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("Younes");
        utilisateur1.setLastname("Oumakhou");
        utilisateur1.setFirstname("Younes");
        utilisateur1.setGender("male");
  
        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("John");
        utilisateur2.setLastname("Doee");
        utilisateur2.setFirstname("John");
        utilisateur2.setGender("male");
  
        Compte emetteur = new Compte();
        emetteur.setNrCompte("999");
        emetteur.setRib("123456789");
        emetteur.setSolde(new BigDecimal(1000));
        emetteur.setUtilisateur(utilisateur1);
  
        Compte beneficiaire = new Compte();
        beneficiaire.setNrCompte("888");
        beneficiaire.setRib("0123456789");
        beneficiaire.setSolde(new BigDecimal(1000));
        beneficiaire.setUtilisateur(utilisateur2);
  
      Transfer transfer = new Transfer();
        transfer.setMotifTransfer("motif");
        transfer.setMontantTransfer(new BigDecimal(500));
        transfer.setCompteEmetteur(emetteur);
        transfer.setCompteBeneficiaire(beneficiaire);
  

        Assertions.assertThat(transfer.getCompteEmetteur().getSolde()).isEqualTo(new BigDecimal(1000));
        Assertions.assertThat(transfer.getCompteBeneficiaire().getSolde()).isEqualTo(new BigDecimal(1000));
        Assertions.assertThat(transfer.getCompteEmetteur().getSolde().subtract(transfer.getMontantTransfer())).isEqualTo(new BigDecimal(500));
        Assertions.assertThat(transfer.getCompteBeneficiaire().getSolde().add(transfer.getMontantTransfer())).isEqualTo(new BigDecimal(1500));

    
    //   Assertions.assertThat(transferRepository.findAll()).hasSize(1);
    //   Assertions.assertThat(transferRepository.findAll().get(0).getMotifTransfer()).isEqualTo("motif");
    //   Assertions.assertThat(transferRepository.findAll().get(0).getMontantTransfer()).isEqualTo(new BigDecimal(500));
    //   Assertions.assertThat(transferRepository.findAll().get(0).getCompteEmetteur()).isEqualTo(emetteur);
    //   Assertions.assertThat(transferRepository.findAll().get(0).getCompteBeneficiaire()).isEqualTo(beneficiaire);
  
    }



}
