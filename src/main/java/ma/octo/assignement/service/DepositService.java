package ma.octo.assignement.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;


@Service
@Transactional
public class DepositService {
    
    public static final int MONTANT_MAXIMAL = 10000;
    
    private final DepositRepository depositRepo;
    private final CompteRepository compteRepo;    
    private final AuditService auditService;

    DepositService(DepositRepository depositRepo, CompteRepository compteRepo, AuditService auditService) {
        this.depositRepo = depositRepo;
        this.compteRepo = compteRepo;
        this.auditService = auditService;
    }

    public void addDeposit(DepositDto depositDto) throws CompteNonExistantException, TransactionException {
        
        Compte compteBeneficiaire = compteRepo.findByRib(depositDto.getRib());
        
        if(compteBeneficiaire == null)
        throw new CompteNonExistantException("RIb n'existe pas dans la base de donnée !");
        
        
        if (depositDto.getMontant().intValue() <= 0)
        throw new TransactionException("Le montant doit être supérieur à 0");
        
        if(depositDto.getMontant().intValue() > MONTANT_MAXIMAL)
        throw new TransactionException("Le montant maximal autorisé est de " + MONTANT_MAXIMAL);
        
        int countPerDate = depositRepo.countDeposits(compteBeneficiaire.getRib());
        
        if (countPerDate > 10) {
            throw new TransactionException("vous avez depasser le nombre maximale  de deposit par jour à ce rib");

        }
        
        
        
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(depositDto.getMontant()));

        MoneyDeposit deposit = new MoneyDeposit();
        deposit.setDateExecution(depositDto.getDate());
        deposit.setCompteBeneficiaire(compteBeneficiaire);
        deposit.setNomPrenomEmetteur(depositDto.getNomPrenomEmetteur());
        deposit.setMotifDeposit(depositDto.getMotif());
        deposit.setMontant(depositDto.getMontant());

        depositRepo.save(deposit);


        auditService.auditDeposit(
            "Deposit vers " + compteBeneficiaire.getNrCompte()
            + ", d'un montant = " + depositDto.getMontant().toString()
    );

    
    }


    public List<MoneyDeposit> allDeposits() {
        List<MoneyDeposit> deposits = depositRepo.findAll();
        return deposits;
    }







}
