package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;



@Service
@Transactional
public class TransferService {
    
    public static final int MONTANT_MAXIMAL = 10000;

    @Autowired
    private final TransferRepository transferRepo;
    @Autowired
    private final CompteRepository compteRepo;
    
    private final AuditService auditService;

    TransferService(TransferRepository transferRepo, CompteRepository compteRepo, AuditService auditService) {
        this.transferRepo = transferRepo;
        this.compteRepo = compteRepo;
        this.auditService = auditService;
    }

    public void addDeposit(TransferDto transferDto) throws CompteNonExistantException, SoldeDisponibleInsuffisantException, TransactionException {
        
        
        Compte compteEmetteur = compteRepo.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepo.findByNrCompte(transferDto.getNrCompteBeneficiaire());    

        if(compteBeneficiaire==null || compteEmetteur==null) throw new CompteNonExistantException("Compte n'existe pas !");
        
        if(transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            throw new TransactionException("Le montant maximal autorisé est de " + MONTANT_MAXIMAL);
        }

        if (transferDto.getMontant().intValue() == 0)
            throw new TransactionException("Le montant doit être supérieur à 0");

        if (transferDto.getMontant().intValue() < 10)
            throw new TransactionException("Le montant doit être supérieur à 10");

        if (compteEmetteur.getSolde().compareTo(transferDto.getMontant()) < 0) {
            throw new SoldeDisponibleInsuffisantException("Le montant doit être inférieur au solde du compte");
        }

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(transferDto.getMontant()));
        

        Transfer transfer = new Transfer();
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMontantTransfer(transferDto.getMontant());

        
        transferRepo.save(transfer);

        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                        .toString());
    }


    public List<Transfer> allTransfers() {
        List<Transfer> transfers = transferRepo.findAll();
        return transfers;
    }







}
