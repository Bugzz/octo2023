package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DepositDto {

    private String nomPrenomEmetteur;
    private String rib;
    private String motif;

    private BigDecimal montant;
    private Date date = new Date();

    public String getNomPrenomEmetteur() {
        return nomPrenomEmetteur;
    }

    public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
        this.nomPrenomEmetteur = nomPrenomEmetteur;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }


    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
