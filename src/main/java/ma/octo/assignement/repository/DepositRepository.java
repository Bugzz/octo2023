package ma.octo.assignement.repository;

import ma.octo.assignement.domain.MoneyDeposit;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DepositRepository extends JpaRepository<MoneyDeposit, Long> {

    // @Query("SELECT COUNT(d) FROM DEP d WHERE d.compteBeneficiaire.nrCompte=:nr_compte AND d.dateExecution <:date")
    @Query(value = "SELECT count(d) FROM DEP d where d.compteBeneficiaire.rib = :rib AND DATE(d.dateExecution)=CURRENT_DATE", nativeQuery = false)
    public int countDeposits(@Param("rib") String rib);

}
