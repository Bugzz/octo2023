package ma.octo.assignement.web;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;

import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("transfers")
class TransferController {

    private final TransferService transferService;

    TransferController(TransferService transferService) {
        this.transferService = transferService;
    }
    
    @GetMapping("/")
    List<Transfer> all() {
        return transferService.allTransfers();
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    void newDeposit(@RequestBody TransferDto depositDto) throws CompteNonExistantException, SoldeDisponibleInsuffisantException, TransactionException {
        transferService.addDeposit(depositDto);
    }
}
