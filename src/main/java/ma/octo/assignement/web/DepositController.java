package ma.octo.assignement.web;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.DepositService;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("deposits")
public class DepositController {
    
    private final DepositService depositService;

    DepositController(DepositService depositService) {
        this.depositService = depositService;
    }
    
    @GetMapping("/")
    List<MoneyDeposit> all() {
        return depositService.allDeposits();
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    void newDeposit(@RequestBody DepositDto depositDto) throws CompteNonExistantException, TransactionException {
        depositService.addDeposit(depositDto);
    }

    
}

