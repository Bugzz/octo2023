package ma.octo.assignement.web;

import java.util.List;

import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;

@RestController
@RequestMapping("comptes")
public class AccountController {
    
    private final CompteRepository compteRepo;


    AccountController(CompteRepository compteRepo) {
        this.compteRepo = compteRepo;
    }

    @GetMapping("/")
    List<Compte> all() {
        List<Compte> comptes = compteRepo.findAll();

        if (CollectionUtils.isEmpty(comptes)) {
            return null;
        } else {
            return comptes;
        }
    }
    
}
