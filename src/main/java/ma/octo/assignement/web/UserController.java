package ma.octo.assignement.web;

import java.util.List;

import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;

@RestController
@RequestMapping("utilisateurs")
public class UserController {
    
    private final UtilisateurRepository userRepo;


    UserController(UtilisateurRepository userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping("/")
    List<Utilisateur> all() {
        List<Utilisateur> utilisateurs = userRepo.findAll();

        if (CollectionUtils.isEmpty(utilisateurs)) {
            return null;
        } else {
            return utilisateurs;
        }
    }
    
}
