package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;

public class DepositMapper {

    private static DepositDto depositDto;

    public static DepositDto map(MoneyDeposit deposit) {
        depositDto = new DepositDto();
        depositDto.setNomPrenomEmetteur(deposit.getNomPrenomEmetteur());
        depositDto.setDate(deposit.getDateExecution());
        depositDto.setMotif(deposit.getMotifDeposit());
        depositDto.setRib(deposit.getCompteBeneficiaire().getRib());

        return depositDto;
    }

}